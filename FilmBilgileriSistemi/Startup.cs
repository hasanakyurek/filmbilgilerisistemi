﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FilmBilgileriSistemi.Startup))]
namespace FilmBilgileriSistemi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
