﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FilmBilgileriSistemi.Areas.Admin.Controllers
{
    public class SliderController : Controller
    {
        FilmBilgileriEntities DB = new FilmBilgileriEntities();
       
        // GET: Admin/Slider
        public ActionResult Index()
        {
           var model= DB.Slider_Table.OrderByDescending( x => x.ID).ToList();
            return View(model);
        }
        public ActionResult Add()
        {
            return View();
        }
        public ActionResult AddSlider(Slider_Table model)
        {
            if(ModelState.IsValid)
            {
                DB.Slider_Table.Add(model);
                DB.SaveChanges();
            }
            return RedirectToAction("Index");
            return View();
        }
    }
}